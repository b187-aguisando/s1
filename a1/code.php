<?php

function getFullAddress($country, $province, $city, $specific){
	return "$specific, $city, $province, $country";
}

function getLetterGrade($grade){
	$word = "is equevalent to";

	if ($grade >= 98 && $grade <= 100) {
		return "$grade $word A+";
	} else if ($grade >= 95 && $grade <= 97) {
		return "$grade $word A";
	} else if ($grade >= 92 && $grade <= 94) {
		return "$grade $word A-";
	} else if ($grade >= 89 && $grade <= 91) {
		return "$grade $word B+";
	} else if ($grade >= 86 && $grade <= 88) {
		return "$grade $word B";
	} else if ($grade >= 83 && $grade <= 85) {
		return "$grade $word B-";
	} else if ($grade >= 80 && $grade <= 82) {
		return "$grade $word C+";
	} else if ($grade >= 77 && $grade <= 79) {
		return "$grade $word C";
	} else if ($grade >= 75 && $grade <= 76) {
		return "$grade $word C-";
	} else if ($grade <= 74) {
		return "$grade $word F";
	}
}